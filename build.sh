#!/bin/bash

############################################################
# VARIABLES ################################################
############################################################
orig_dir="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"
magento_dir="magento_mirror"
build_dir="build"
user="$SUDO_USER" #not used
group="$SUDO_USER" #not used
magento_version="magento-1.9"
magento_version_current="$(cat magento-version 2> /dev/null)"
#place in
#/pre-build.run
#/build.run
#/post-build.run
#/pre-deploy.run
#/deploy.run
#/post-deploy.run
cmd_pre_run=""
cmd_run=""
cmd_post_run=""

action="none"
############################################################


############################################################
# FUNCTIONS ################################################
############################################################

# run function
# @param $1; commands to execute
function run {
	cmd="$1"

#	if [ ! -z "$SUDO_USER" ]
#	then
#		eval "sudo su - $user -s /bin/bash -c \"$cmd\""
#	else
		eval "$cmd"
#	fi
}

# build_init function
function build_init {
	if [ -z "$cmd_pre_run" ]
	then
		cmd_pre_run="$(cat $orig_dir/pre-build.run 2> /dev/null)"
	fi
	
	if [ -z "$cmd_run" ]
	then
		cmd_run="$(cat $orig_dir/build.run 2> /dev/null)"
	fi

	if [ -z "$cmd_post_run" ]
	then
		cmd_post_run="$(cat $orig_dir/post-build.run 2> /dev/null)"
	fi

	# fetch and init & update submodules
	cd "$orig_dir"
	git fetch
	git submodule init && git submodule update && git submodule foreach 'git submodule init && git submodule update'
}

# build_add_magento function
function build_add_magento {

	# magento version to build with
	cd "$orig_dir/$magento_dir"
	git fetch
	git checkout "$magento_version"

	#clean build if magento version mismatch
#	if [ "$magento_version_current" != "$(git branch | sed -n 's/^*\s(detached from //p; s/)$//p')" \
#	&&	 "$magento_version_current" != "$(git branch | sed -n 's/^*\s//p')" ]
	if [ "$magento_version_current" != "$magento_version" ]
	then
		clean 0

		#set new magento-version
		echo "$magento_version" > "$orig_dir/magento-version"
	else
		echo "magento_version unchanged ($magento_version)"
	fi

	# pull/merge changes from magento-mirror
	git pull

	# copy to build directory
	cd "$orig_dir"
	rsync -a "$orig_dir/$magento_dir/" "$orig_dir/$build_dir/" --exclude='.git*'
}

#build
function build {
	############################################################
	# PRE-BUILD ################################################
	############################################################
	# init & update git submodules
	build_init

	# execute pre-build command
	if [ ! -z "$cmd_pre_run" ]
	then
		run "$cmd_pre_run"
	fi
	############################################################

	############################################################
	# BUILD ####################################################
	############################################################

	#rsync magento version to build directory
	build_add_magento

	# execute build command
	if [ -z "$cmd_run" ]
	then
		cmd_run="composer install"
	fi

	run "$cmd_run"

	############################################################

	############################################################
	# POST-BUILD ###############################################
	############################################################

	# execute post-build command
	if [ ! -z "$cmd_post_run" ]
	then
		run "$cmd_post_run"
	fi
	############################################################
}

# clean function
function clean {
	exit_after_clean="$1"

	if [ -z "$exit_after_clean" ]
	then
		exit_after_clean=1
	fi

	#remove & recreate build directory
	if [ -d "$orig_dir/$build_dir" ]
	then
		rm -rf "$orig_dir/$build_dir"
	fi
	mkdir "$orig_dir/$build_dir"
	mkdir "$orig_dir/$build_dir/lib"

	#remove magento-version
	rm -rf "$orig_dir/magento-version" 2> /dev/null

	echo "*** cleaned $orig_dir/$build_dir ***"

	if [ "$exit_after_clean" -eq 1 ]
	then
		exit
	fi
}

#deploy init
function deploy_init {
	if [ -z "$cmd_pre_run" ]
	then
		cmd_pre_run="$(cat $orig_dir/pre-deploy.run 2> /dev/null)"
	fi
	
	if [ -z "$cmd_run" ]
	then
		cmd_run="$(cat $orig_dir/deploy.run 2> /dev/null)"
	fi

	if [ -z "$cmd_post_run" ]
	then
		cmd_post_run="$(cat $orig_dir/post-deploy.run 2> /dev/null)"
	fi
}

#deploy
function deploy {
	############################################################
	# PRE-DEPLOY ###############################################
	############################################################

	deploy_init

	# execute pre-build command
	if [ ! -z "$cmd_pre_run" ]
	then
		run "$cmd_pre_run"
	fi
	############################################################

	############################################################
	# DEPLOY ###################################################
	############################################################

	# execute deploy command
	if [ -z "$cmd_run" ]
	then
		echo "************************************************"
		echo "deploy requires cmd_run to be set"
		echo "************************************************"
		exit
	fi

	run "$cmd_run"

	############################################################

	############################################################
	# POST-DEPLOY ##############################################
	############################################################

	# execute post-build command
	if [ ! -z "$cmd_post_run" ]
	then
		run "$cmd_post_run"
	fi
	############################################################
}

# output_help function
function output_help {
	echo
	echo "usage: build.sh -a <build|clean|help> \
[-b <cmd_run>] \
[-d <build_dir>] \
[-g <group>] \
[-m <magento version>] \
[-p <cmd_pre_run>] \
[-P <cmd_post_run>] \
[-u <user>]"
	echo
	echo "required param: -a"
	echo "build = builds project"
	echo "clean = cleans project"
	echo "help = this help"
	echo
	echo "optional params:"
	echo "-b <cmd_run> = commands or scripts to run during build; default is 'composer install'"
	echo "-d <build_dir> = build directory; default is 'build'"
	echo "-g <group> = group to use if needed; default is '\$user'"
	echo "-m <magento version> = magento version to use; default is 'magento-1.9'" 
	echo "-p <cmd_pre_run> = commands or scripts to run before build"
	echo "-P <cmd_post_run> = commands or scripts to run after build"
	echo "-u <user> = user to use if needed; default is '\$SUDO_USER' or '\$USER'"
	echo
}

#capture params
function capture_params {
	############################################################
	# GLOBAL PARAMS ############################################
	############################################################

	#set optional globals
	while getopts ":a:c:d:g:m:u:p:P:" opt; do
	  case $opt in
	  	a) action="$OPTARG"
		;;
	    c) cmd_run="$OPTARG"
	    ;;
	    d) build_dir="$OPTARG"
	    ;;
	    g) group="$OPTARG"
			if [ -z "$(getent group $group)" ]
			then
				echo "************************************************"
				echo "group $group not found exiting"
				echo "************************************************"
				exit
			else
				echo "group exists"
			fi
		;;
	    m) magento_version="$OPTARG"
			cd "$orig_dir/$magento_dir"

			#check if magento version exists
			if [ -z "$(git branch -a | grep $magento_version)" ] \
			&& [ -z "$(git tag -l | grep $magento_version)" ] \
			&& [ -z "$(git log | grep $magento_version)" ]
			then
				echo "************************************************"
				echo "$magento_version not found in repository exiting"
				echo "************************************************"
				exit
			fi
			cd "$orig_dir"
		;;
		p) cmd_pre_run="$OPTARG"
		;;
		P) cmd_post_run="$OPTARG"
		;;
		u) user="$OPTARG"
			if [ -z "$(getent passwd $user)" ]
			then
				echo "************************************************"
				echo "user $user not found exiting"
				echo "************************************************"
				exit
			else
				echo "user exists"
			fi
	    ;;
	    \?) echo "Invalid option -$OPTARG" >&2
	    ;;
	  esac
	done

	#make sure user is set
	if [ -z "$user" ]
	then
		user="$USER"
	fi

	#make sure group is set
	if [ -z "$group" ]
	then
		group="$user"
	fi

	#make sure user belongs to group
	if [ -z "$(groups $user | grep $group)" ]
	then
		echo "************************************************"
		echo "user $user does not belong to group $group exiting"
		echo "************************************************"
		exit
	fi
	############################################################	
}

#output params
function output_params {
	echo "*****************************************************"
	echo "* PARAMETERS USED ***********************************"
	echo "*****************************************************"
	echo
	echo "action=$action"
	echo
	echo "build_dir=$build_dir"
	echo "magento_version_current=$magento_version_current"
	echo "magento_version=$magento_version"
	echo "user=$user"
	echo "group=$group"
	echo "cmd_pre_run=$cmd_pre_run"
	echo "cmd_run=$cmd_run"
	echo "cmd_post_run=$cmd_post_run"
	echo "*****************************************************"
	echo
}

#main
function main {
	#set variables
	capture_params $@

	#set action global
	case "$action" in
			build)
				action="build"
				output_params
				build
				;;
	        clean)
	            action="clean"
	            output_params
	            clean
	            ;;
	        deploy)
				action="deploy"
				output_params
				deploy
				;;
	        #help & exit
	        *)
				output_help
				exit
	 			;;
	esac
}
############################################################

#execute main!
main $@
